# pixiv-flask
Alternative frontend for Pixiv written in Flask, currently WIP.
## Implemented features
- [x] Recommended works
- [x] Auto-complete search
- [x] Search (only illusts)
- [x] Illustration page
- [x] Ugoira
## TODO
- [ ] User page
- [ ] Bookmark page
- [ ] Manga search
- [ ] Everything else that is not implemented that I need to reverse engineer the Android API for
- [ ] Make my code less ugly
- [ ] Make the UI less ugly
- [ ] Make my own simple library to read the Ugoira's zip file instead of using JSZip