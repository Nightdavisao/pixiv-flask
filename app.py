from flask import Flask, render_template, Response, request, send_from_directory
from client import PixivClient
import requests
import json
import urllib.parse
from datetime import datetime
from werkzeug.exceptions import HTTPException

app = Flask(__name__)

pixiv_client = PixivClient.from_json("pixiv_cred.json")

# helper function for the templates to get the url of the proxy
@app.context_processor
def utility_processor():
    def proxy_url(url):
        return f"/proxy/{url}"
    def parse_timedate(time):
        return datetime.strptime(time, "%Y-%m-%dT%H:%M:%S%z").strftime("%Y-%m-%d %H:%M:%S")
    def user_prefs():
        return get_user_prefs()
    return dict(proxy_url=proxy_url, parse_timedate=parse_timedate, user_prefs=user_prefs)

@app.errorhandler(Exception)
def handle_exception(e):
    if isinstance(e, HTTPException):
        return e
    
    return render_template('500_generic.jinja', error=e), 500

# serve static files
@app.route('/static/<path:path>')
def send_static(path):
    return send_from_directory('static', path)

@app.route('/')
def index():
    return render_template('index.jinja', illusts=pixiv_client.get_recommended().json()["illusts"])

USER_DEFAULT_PREFERENCES = {
    "default_sort": "popular_desc",
    "enable_javascript": True,
    "enable_ugoira": True,
    "enable_autocomplete": True,
}

@app.route('/pref', methods=["GET", "POST"])
def preferences():
    if request.method == "GET":
        user_prefs = urllib.parse.unquote(request.cookies.get("prefs"))
        if user_prefs is None:
            user_prefs = USER_DEFAULT_PREFERENCES
        else:
            user_prefs = json.loads(user_prefs)

        return render_template('pref.jinja', prefs=user_prefs)
    else:
        app.logger.info(f"updating user preferences: {request.form}")
        prefs = {}
        for key in USER_DEFAULT_PREFERENCES.keys():
            # infer the type of the value
            if type(USER_DEFAULT_PREFERENCES[key]) == bool:
                prefs[key] = request.form.get(key) == "on"
            else:
                prefs[key] = request.form.get(key)
        resp = app.make_response(render_template('pref.jinja', prefs=prefs, success=True))
        resp.set_cookie("prefs", urllib.parse.quote(json.dumps(prefs)))
        return resp
    
def get_user_prefs():
    user_prefs = urllib.parse.unquote(request.cookies.get("prefs"))
    if user_prefs is None:
        return USER_DEFAULT_PREFERENCES
    else:
        return json.loads(user_prefs)

@app.route('/search')
def search():
    keyword = request.args.get("q")
    sort_option = request.args.get("s")
    # if not specified, get the default sort option from the user preferences
    if sort_option is None:
        sort_option = get_user_prefs()["default_sort"]
    if sort_option == "popular_desc" and not pixiv_client.is_premium:
        print("user is not premium, using the preview endpoint")
        illusts = pixiv_client.search_illust_popular(params={"word": keyword}).json()["illusts"]
    else:
        illusts = pixiv_client.search_illust(params={"word": keyword, "sort": sort_option}).json()["illusts"]
    return render_template('search.jinja', keyword=keyword, sort_option=sort_option, illusts=illusts)

@app.route('/search/autocomplete')
def search_autocomplete():
    keyword = request.args.get("keyword")
    return pixiv_client.get_search_autocomplete(params={"word": keyword}).json()

@app.route('/ugoira/metadata')
def ugoira_metadata():
    illust_id = request.args.get("illust_id")
    return pixiv_client.get_ugoira_metadata(params={"illust_id": illust_id}).json()

@app.route('/artworks/<illust_id>')
@app.route('/en/artworks/<illust_id>/')
def artworks(illust_id):
    illust = pixiv_client.get_illust_detail(params={"illust_id": illust_id}).json()["illust"]
    related_illusts = pixiv_client.get_illust_related(params={"illust_id": illust_id}).json()["illusts"]
    return render_template('illust.jinja', illust=illust, related_illusts=related_illusts)

@app.route('/proxy/<path:image_url>')
def proxy(image_url):
    app.logger.info(f"proxying {image_url}")

    if not image_url.startswith("https://i.pximg.net"):
        raise Exception("Invalid image url")
    
    req = requests.get(image_url, headers={
        "user-agent": "PixivAndroidApp/6.85.0 (Android 13; Pixel 5)",
        "referer": "https://app-api.pixiv.net/",
    }, stream=True)
    req.raise_for_status()

    def generate():
        for chunk in req.iter_content(chunk_size=1024):
            yield chunk

    return Response(generate(), status=req.status_code, headers=req.headers.items())

