(function () {
    const API = {
        searchAutocomplete: (keyword) => {
            const url = new URL('/search/autocomplete', window.location.origin);
            url.searchParams.append('keyword', keyword);

            return fetch(url)
                .then(response => response.json())
                .then(data => data)
        },
        ugoiraMetadata: (illustId) => {
            const url = new URL('/ugoira/metadata', window.location.origin);
            url.searchParams.append('illust_id', illustId);

            return fetch(url)
                .then(response => response.json())
                .then(data => data)
        }
    }

    function getPreferences() {
        const cookie = document.cookie.split(';').find(cookie => cookie.startsWith('prefs='));
        if (cookie) {
            const json = decodeURIComponent(cookie.split('=')[1])
            return JSON.parse(json);
        }
    }

    function proxyUrl(url) {
        return new URL(`/proxy/${url}`, window.location.origin);
    }

    async function handleAutocomplete() {
        // create autocomplete element
        const autocomplete = document.createElement('div');
        autocomplete.classList.add('autocomplete');
        autocomplete.style.display = 'none';
        autocomplete.style.position = 'absolute';
        autocomplete.style.padding = '5px';
        autocomplete.style.backgroundColor = 'white';
        autocomplete.style.color = 'black';
        autocomplete.style.zIndex = '5';
        autocomplete.style.userSelect = 'none';

        function hideAutocomplete() {
            autocomplete.innerHTML = '';
            autocomplete.style.display = 'none';
        }

        const searchInput = document.getElementById('search-bar');
        let focusPosition = -1
        searchInput.parentNode.appendChild(autocomplete);
        // listen for input event on searchInput
        searchInput.addEventListener('input', async () => {
            let inputValue = searchInput.value;
            if (inputValue.length > 0) {
                try {
                    const data = await API.searchAutocomplete(inputValue)
                    autocomplete.style.display = 'block';
                    console.log(data)
                    const tags = data["tags"]
                    autocomplete.innerHTML = '';
                    tags.forEach(tag => {
                        const autocompleteItem = document.createElement('div');
                        autocompleteItem.classList.add('autocomplete-item');
                        autocompleteItem.innerText = `${tag.name}${tag.translated_name ? " - " + tag.translated_name : ''}`
                        autocompleteItem.addEventListener('click', () => {
                            searchInput.value = tag.name;
                            autocomplete.innerHTML = '';
                        })
                        autocomplete.appendChild(autocompleteItem);
                    })
                } catch (error) {
                    console.log(error)
                }
            } else {
                hideAutocomplete();
            }
        })

        // listen for blur event on searchInput
        searchInput.addEventListener('blur', () => {
            setTimeout(() => {
                hideAutocomplete();
            }, 100)
        })

        // listen for keyup event on searchInput
        searchInput.addEventListener('keydown', (event) => {
            const autocompleteItems = autocomplete.querySelectorAll('.autocomplete-item');
            if (event.key === 'ArrowDown') {
                focusPosition++;
                if (focusPosition > autocompleteItems.length - 1) {
                    focusPosition = 0;
                }
                autocompleteItems.forEach(item => item.classList.remove('autocomplete-focus'));
                autocompleteItems[focusPosition].classList.add('autocomplete-focus');
            } else if (event.key === 'ArrowUp') {
                focusPosition--;
                if (focusPosition < 0) {
                    focusPosition = autocompleteItems.length - 1;
                }
                autocompleteItems.forEach(item => item.classList.remove('autocomplete-focus'));
                autocompleteItems[focusPosition].classList.add('autocomplete-focus');
            } else if (event.key === 'Enter') {
                if (focusPosition === -1 || autocompleteItems.length === 0) {
                    return;
                }
                event.preventDefault();
                autocompleteItems[focusPosition].click();
                hideAutocomplete();
            }
        })

        // listen for hover event on autocomplete
        autocomplete.addEventListener('mouseover', (event) => {
            const autocompleteItems = autocomplete.querySelectorAll('.autocomplete-item');
            if (event.target.classList.contains('autocomplete-item')) {
                autocompleteItems.forEach(item => item.classList.remove('autocomplete-focus'));
                event.target.classList.add('autocomplete-focus');
                focusPosition = Array.from(autocompleteItems).indexOf(event.target);
            }
        })
    }

    async function handleUgoira() {
        const timer = ms => new Promise(res => setTimeout(res, ms))

        const ugoiraIllust = document.querySelector('.ugoira-illust');
        if (ugoiraIllust) {
            ugoiraIllust.style.cursor = 'pointer';
            ugoiraIllust.addEventListener('click', async () => {
                try {
                    ugoiraIllust.style.cursor = 'default';
                    ugoiraIllust.style.userSelect = 'none';
                    ugoiraIllust.innerHTML = 'Fetching ugoira metadata...';

                    const illustId = ugoiraIllust.dataset.illustId
                    const metadata = await API.ugoiraMetadata(illustId)
                    const ugoiraMetadata = metadata["ugoira_metadata"]
                    const frames = ugoiraMetadata["frames"]

                    const zipUrl = proxyUrl(ugoiraMetadata["zip_urls"]["medium"])

                    ugoiraIllust.innerHTML = 'Fetching ugoira zip file...';
                    const zipFile = await fetch(zipUrl).then(res => res.arrayBuffer())

                    ugoiraIllust.innerHTML = 'Loading ugoira zip file...';
                    const zip = await JSZip.loadAsync(zipFile)

                    ugoiraIllust.innerHTML = 'Loading ugoira images...';
                    const imageFiles = Object.values(zip.files).filter(file => file.name.endsWith('.jpg'))
                    const images = await Promise.all(imageFiles.map(function (file) {
                        return new Promise(async (resolve, reject) => {
                            const image = document.createElement('img')
                            // wait for image to load
                            image.addEventListener('load', () => resolve(image))
                            // if image fails to load, reject promise
                            image.addEventListener('error', () => reject())
                            image.src = await file.async('blob').then(blob => URL.createObjectURL(blob))
                        })
                    }))

                    ugoiraIllust.innerHTML = 'Loading ugoira canvas...';
                    // create canvas element
                    const canvas = document.createElement('canvas')
                    canvas.width = images[0].width
                    canvas.height = images[0].height
                    // append canvas to ugoiraIllust
                    ugoiraIllust.innerHTML = ''
                    
                    const loopCheckbox = document.createElement('input')
                    loopCheckbox.type = 'checkbox'
                    loopCheckbox.classList.add('ugoira-loop-checkbox')
                    loopCheckbox.checked = true
                    let shouldLoop = loopCheckbox.checked

                    const loopLabel = document.createElement('label')
                    loopLabel.innerText = 'Loop'

                    loopCheckbox.addEventListener('change', () => {
                        shouldLoop = loopCheckbox.checked
                    })
                    ugoiraIllust.appendChild(loopCheckbox)
                    ugoiraIllust.appendChild(loopLabel)
                    ugoiraIllust.append(document.createTextNode(' '))
                    ugoiraIllust.appendChild(document.createTextNode('Click the ugoira to play/pause'))
                    ugoiraIllust.appendChild(document.createElement('br'))

                    ugoiraIllust.appendChild(canvas)

                    let paused = false
                    let isPlaying = false
                    let frameIndex = 0

                    canvas.addEventListener('click', () => {
                        if (isPlaying) {
                            const wasPaused = paused
                            paused = !paused
                            if (wasPaused && !paused) {
                                draw()
                            }
                        } else {
                            draw()
                        }
                    })

                    async function draw() {
                        isPlaying = true
                        while (frameIndex < frames.length) {
                            if (!paused) {
                                const frame = frames[frameIndex]
                                const delay = frame["delay"]
                                const image = images[frameIndex]
                                const ctx = canvas.getContext('2d')
                                ctx.clearRect(0, 0, canvas.width, canvas.height)
                                ctx.drawImage(image, 0, 0)
                                await timer(delay)
                                frameIndex++
                            } else {
                                return
                            }
                        }
                        frameIndex = 0
                        if (shouldLoop) {
                            draw()
                        } else {
                            isPlaying = false
                        }
                    }

                    draw()
                } catch (error) {
                    console.log(error)
                    ugoiraIllust.innerHTML = 'Failed to load ugoira'
                }
            }, { once: true })
        }
    }

    // load preferences from cookies
    const preferences = getPreferences()
    console.log(preferences)

    document.addEventListener('DOMContentLoaded', async () => {
        Promise.all([
            preferences["enable_autocomplete"] ? handleAutocomplete() : Promise.resolve(),
            preferences["enable_ugoira"] ? handleUgoira() : Promise.resolve(),
        ]).catch(error => console.log(error))
    })
})();