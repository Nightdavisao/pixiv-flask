# pixiv client class

import requests
import json

PIXIV_JSON_FILE = "pixiv_cred.json"

class PixivClient:
    def __init__(self, access_token, refresh_token, current_user):
        self.access_token = access_token
        self.refresh_token = refresh_token
        self.account = current_user.get("account") or ""
        self.user_id = current_user.get("id") or 0
        self.user_name = current_user.get("name") or ""
        self.is_premium = current_user.get("is_premium") or False

        self.user_agent = "PixivAndroidApp/6.85.0 (Android 13; Pixel 5)"
        self.common_headers = {
            "User-Agent": self.user_agent,
            "Content-Type": "application/x-www-form-urlencoded; charset=UTF-8",
            "Accept-Language": "en_US",
            "App-OS": "android",
            "App-OS-Version": "13",
            "App-Version": "6.85.0",
        }

    # static method which returns a PixivClient instance from a json file
    @staticmethod
    def from_json(json_file):
        with open(json_file) as f:
            data = json.load(f)
            current_user = data.get("current_user") or {}
            return PixivClient(data["access_token"], data["refresh_token"], current_user=current_user)
        
    # static method which dumps a PixivClient instance to a json file
    @staticmethod
    def to_json(json_file, client):
        with open(json_file, "w") as f:
            json.dump({
                "access_token": client.access_token,
                "refresh_token": client.refresh_token,
                "current_user": {
                    "account": client.account,
                    "id": client.user_id,
                    "name": client.user_name,
                    "is_premium": client.is_premium,
                }
            }, f)

    def refresh_access_token(self):
        res = requests.post("https://oauth.secure.pixiv.net/auth/token", data={
            "client_id": "MOBrBDS8blbauoSck0ZfDbtuzpyT",
            "client_secret": "lsACyCD94FhDUtGTXi3QzcFE2uU1hqtDaKeqrdwj",
            "grant_type": "refresh_token",
            "refresh_token": self.refresh_token,
        }, headers=self.common_headers)
        if res.status_code != 200:
            raise Exception("Failed to refresh access token")
        data = res.json()
        self.access_token = data["response"]["access_token"]
        self.refresh_token = data["response"]["refresh_token"]
        self.account = data["response"]["user"]["account"]
        self.user_id = data["response"]["user"]["id"]
        self.user_name = data["response"]["user"]["name"]
        self.is_premium = data["response"]["user"]["is_premium"]

        # save to json file
        PixivClient.to_json(PIXIV_JSON_FILE, self)

    def get(self, url, params={}):
        try:
            req = requests.get(url, params=params, headers={
                "Authorization": f"Bearer {self.access_token}",
            } | self.common_headers)
            req.raise_for_status()
            return req
        except requests.exceptions.HTTPError as e:
            if e.response.status_code == 400:
                # is it because of expired access token?
                if "invalid_grant" in e.response.json()["error"]["message"]:
                    print("Access token expired")
                    # refresh access token
                    self.refresh_access_token()
                    return self.get(url, params)
                else:
                    print(e.response.json())
                    raise e
            else:
                raise e

    def get_current_user_detail(self):
        return self.get_user_detail({"user_id": self.user_id})

    def get_user_detail(self, params={}):
        return self.get("https://app-api.pixiv.net/v1/user/detail", params=params)
            
    def search_illust(self, params={}):
        default_params = {
            "filter": "for_android",
            "sort": "date_desc",
            "search_target": "partial_match_for_tags",
            "merge_plain_keyword_results": "true",
            "include_translated_tag_results": "true",
        }
        print(default_params | params)
        return self.get("https://app-api.pixiv.net/v1/search/illust", params=default_params | params)
    
    def search_illust_popular(self, params={}):
        default_params = {
            "filter": "for_android",
            "search_target": "partial_match_for_tags",
            "merge_plain_keyword_results": "true",
            "include_translated_tag_results": "true",
        }
        return self.get("https://app-api.pixiv.net/v1/search/popular-preview/illust", params=default_params | params)
            
    def get_search_autocomplete(self, params={}):
        return self.get("https://app-api.pixiv.net/v2/search/autocomplete", params=params)

    def get_recommended(self, params={
        "include_ranking_illusts": "true",
        "include_privacy_policy": "true",
        "filter": "for_android",
    }):
        return self.get("https://app-api.pixiv.net/v1/illust/recommended", params=params)

    def get_follow(self, params={}):
        return self.get("https://app-api.pixiv.net/v2/illust/follow", params=params)

    def get_ranking(self, params={}):
        return self.get("https://app-api.pixiv.net/v1/illust/ranking", params=params)

    def get_bookmark(self, params={}):
        return self.get("https://app-api.pixiv.net/v1/user/bookmarks/illust", params=params)

    def get_user_illusts(self, params={}):
        return self.get("https://app-api.pixiv.net/v1/user/illusts", params=params)

    def get_user_bookmarks_illust(self, params={}):
        return self.get("https://app-api.pixiv.net/v1/user/bookmarks/illust", params=params)

    def get_user_detail(self, params={}):
        return self.get("https://app-api.pixiv.net/v1/user/detail", params=params)

    def get_illust_detail(self, params={}):
        return self.get("https://app-api.pixiv.net/v1/illust/detail", params=params)
    
    def get_ugoira_metadata(self, params={}):
        return self.get("https://app-api.pixiv.net/v1/ugoira/metadata", params=params)

    def get_illust_comments(self, params={}):
        return self.get("https://app-api.pixiv.net/v1/illust/comments", params=params)

    def get_illust_related(self, params={}):
        return self.get("https://app-api.pixiv.net/v2/illust/related", params=params)